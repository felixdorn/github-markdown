<?php

use Felix\Profiler\Profiler;
use Felix\GithubMarkdown\Transformer;
use Symfony\Component\Debug\Debug;
use Symfony\Component\Debug\ErrorHandler;
use Symfony\Component\Debug\ExceptionHandler;

require '../vendor/autoload.php';
ErrorHandler::register();
ExceptionHandler::register();
Debug::enable();
?>
<!DOCTYPE html>
<link rel="stylesheet" href="prism.css">
<link rel="stylesheet" href="markdown.css">
<div class="container" style="width: 70%; margin: 0 auto;">
    <?= Transformer::process(file_get_contents('example.md')) ?>
</div>

<script src="prism.js"></script>
