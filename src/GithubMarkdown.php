<?php


namespace Felix\GithubMarkdown;


use cebe\markdown\block\TableTrait;
use cebe\markdown\inline\StrikeoutTrait;
use cebe\markdown\inline\UrlLinkTrait;
use cebe\markdown\Markdown;
use Emojione\Client;
use Emojione\Ruleset;

class GithubMarkdown extends Markdown
{
    use TableTrait;
    // include inline element parsing using traits
    use StrikeoutTrait;
    use UrlLinkTrait;
    /**
     * @var boolean whether to interpret newlines as `<br />`-tags.
     * This feature is useful for comments where newlines are often meant to be real new lines.
     */
    public $enableNewlines = false;

    public $html5 = true;
    /**
     * @inheritDoc
     */
    protected $escapeCharacters = [
        // from Markdown
        '\\', // backslash
        '`', // backtick
        '*', // asterisk
        '_', // underscore
        '{', '}', // curly braces
        '[', ']', // square brackets
        '(', ')', // parentheses
        '#', // hash mark
        '+', // plus sign
        '-', // minus sign (hyphen)
        '.', // dot
        '!', // exclamation mark
        '<', '>',
        // added by GithubMarkdown
        ':', // colon
        '|', // pipe
    ];


    /**
     * Consume lines for a paragraph
     *
     * Allow headlines, lists and code to break paragraphs
     */
    protected function consumeParagraph($lines, $current)
    {
        // consume until newline
        $content = [];
        for ($i = $current, $count = count($lines); $i < $count; $i++) {
            $line = $lines[$i];
            if ($line === ''
                || ltrim($line) === ''
                || !ctype_alpha($line[0]) && (
                    $this->identifyQuote($line, $lines, $i) ||
                    $this->identifyFencedCode($line, $lines, $i) ||
                    $this->identifyUl($line, $lines, $i) ||
                    $this->identifyOl($line, $lines, $i) ||
                    $this->identifyHr($line, $lines, $i)
                )
                || $this->identifyHeadline($line, $lines, $i)) {
                break;
            } elseif ($this->identifyCode($line, $lines, $i)) {
                // possible beginning of a code block
                // but check for continued inline HTML
                // e.g. <img src="file.jpg"
                //           alt="some alt aligned with src attribute" title="some text" />
                if (preg_match('~<\w+([^>]+)$~s', implode("\n", $content))) {
                    $content[] = $line;
                } else {
                    break;
                }
            } else {
                $content[] = $line;
            }
        }
        $block = [
            'paragraph',
            'content' => $this->parseInline(implode("\n", $content)),
        ];
        return [$block, --$i];
    }

    protected function identifyFencedCode($line): bool
    {
        // if a line starts with at least 3 backticks it is identified as a fenced code block
        if (strncmp($line, '```', 3) === 0) {
            return true;
        }
        return false;
    }


    protected function consumeCodepen($lines, $current)
    {
        $line = rtrim($lines[$current]);
        dd($line);
    }

    /**
     * @marker :
     */
    protected function parseSmiley($markdown)
    {
        // check whether the marker really represents a strikethrough (i.e. there is a closing ~~)
        if (preg_match('/^:(.+?):/', $markdown, $matches)) {
            $emojy = new Client(new Ruleset());
            return [
                // return the parsed tag as an element of the abstract syntax tree and call `parseInline()` to allow
                // other inline markdown elements inside this tag
                ['smiley', $this->parseInline($emojy->shortnameToUnicode($matches[0]))],
                // return the offset of the parsed text
                strlen($matches[0])
            ];
        }
        // in case we did not find a closing ~~ we just return the marker and skip 2 characters
        return [['text', ':'], 1];
    }

    // rendering is the same as for block elements, we turn the abstract syntax array into a string.
    protected function renderSmiley($element)
    {
        return '<del>' . $this->renderAbsy($element[1]) . '</del>';
    }

    /**
     * @inheritdocs
     *
     * Parses a newline indicated by two spaces on the end of a markdown line.
     */
    protected function renderText($text)
    {
        if ($this->enableNewlines) {
            $br = $this->html5 ? "<br>\n" : "<br />\n";
            return strtr($text[1], ["  \n" => $br, "\n" => $br]);
        } else {
            return parent::renderText($text);
        }
    }

    protected function consumeFencedCode($lines, $current)
    {
        // create block array
        $block = [
            'fencedCode',
            'content' => [],
        ];
        $line = rtrim($lines[$current]);
        $html = <<<HTML
        <iframe height="265" style="width: 100%;" scrolling="no" title="CSS3/Javascript Pure Dropdown Menu" src="//codepen.io/pedronauck/embed/preview/fcaDw/?height=265&theme-id=dark&default-tab=result" frameborder="no" allowtransparency="true" allowfullscreen="true">
            See the Pen <a href='https://codepen.io/pedronauck/pen/fcaDw/'>CSS3/Javascript Pure Dropdown Menu</a> by Pedro Nauck
            (<a href='https://codepen.io/pedronauck'>@pedronauck</a>) on <a href='https://codepen.io'>CodePen</a>.
        </iframe>
HTML;

        // detect language and fence length (can be more than 3 backticks)
        $fence = substr($line, 0, $pos = strrpos($line, '`') + 1);
        $language = substr($line, $pos);
        if (!empty($language)) {
            $block['language'] = $language;
        }

        // consume all lines until ```
        for ($i = $current + 1, $count = count($lines); $i < $count; $i++) {
            if (rtrim($line = $lines[$i]) !== $fence) {
                $block['content'][] = $line;
            } else {
                // stop consuming when code block is over
                break;
            }
        }
        return [$block, $i];
    }

    protected function renderFencedCode($block)
    {
        $class = isset($block['language']) ? ' class="language-' . $block['language'] . '"' : '';
        return "<pre><code $class>" . htmlspecialchars(implode("\n", $block['content']) . "\n", ENT_NOQUOTES, 'UTF-8') . '</code></pre>';
    }

}