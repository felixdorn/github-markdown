<?php

namespace Felix\GithubMarkdown;



class Parser
{
    public function parse(string $markdown)
    {
        $parser = new GithubMarkdown();
        $transformed = $parser->parse($markdown);
        $transformed = '<section  id="' . StyleProvider::get('content') . '" class="' . StyleProvider::get('content') . '">' . $transformed . '</section>';
        $document = new \DOMDocument();
        $document->formatOutput = false;
        @$document->loadHTML($transformed);
        return $document;
    }
}