<?php


namespace Felix\GithubMarkdown;


class StyleProvider
{
    public const CLASSES_PREFIX = 'felix-markdown-';

    public static function get(string $type): string
    {
        return self::buildClass($type);
    }

    private static function buildClass(string $type)
    {
        return self::CLASSES_PREFIX . $type;
    }
}