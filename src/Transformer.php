<?php

namespace Felix\GithubMarkdown;

use Cocur\Slugify\Slugify;
use DOMDocument;
use DOMElement;

class Transformer
{


    /**
     * @var string
     */
    private $text;

    public static function process(string $markdown)
    {
        return (new self)->handle($markdown);
    }

    private function handle(string $markdown): string
    {
        $this->text = $markdown;
        $parser = new Parser();
        $html = $parser->parse($markdown);
        $actions = ['code', 'headings', 'images', 'links', 'quotes', 'table'];
        foreach ($actions as $action) {
            $html = $this->{'make' . ucfirst($action)}($html);
        }
        # Ensure that there is no unwanted tag in the output
        return $this->formatHtml($html->saveHTML());
    }

    private function formatHtml(string $html)
    {
        $html = preg_replace('~<(?:!DOCTYPE|/?(?:html|body))[^>]*>\s*~i', '', $html);
        return $html;
    }

    public function makeQuotes(DOMDocument $html): DOMDocument
    {
        $quotes = $html->getElementsByTagName('blockquote');
        /** @var DOMElement $quote */
        foreach ($quotes as $quote) {
            $quote->setAttribute('class', StyleProvider::get('quote'));
        }

        return $html;
    }

    private function makeCode(DOMDocument $html): DOMDocument
    {

        $codes = $html->getElementsByTagName('code');
        /** @var DOMElement $code */
        foreach ($codes as $code) {
            if ($code->parentNode->nodeName !== 'pre') {
                $code->setAttribute('class', StyleProvider::get('shortcode'));
            }
        }
        return $html;
    }

    private function makeStrings(DOMDocument $html): DOMDocument
    {
        /** @var DOMElement $bold */
        foreach ($html->getElementsByTagName('strong') as $bold) {
            $bold->setAttribute('class', StyleProvider::get('bold'));
        }

        return $html;
    }

    private function makeImages(DOMDocument $html): DOMDocument
    {
        $images = $html->getElementsByTagName('img');
        /** @var DOMElement $image */
        foreach ($images as $image) {
            $image->setAttribute('class', StyleProvider::get('image'));
        }
        return $html;
    }

    private function makeHeadings(DOMDocument $html): DOMDocument
    {
        for ($i = 0; $i <= 6; $i++) {
            $headings = $html->getElementsByTagName("h$i");
            /** @var DOMElement $heading */
            foreach ($headings as $heading) {
                $content = $heading->nodeValue;
                $slugyTitle = (new Slugify)->slugify($content);
                $heading->setAttribute('id', $slugyTitle);
                $heading->setAttribute('class', StyleProvider::get("h$i"));
            }
        }
        return $html;
    }

    private function makeTable(DOMDocument $html): DOMDocument
    {
        $tables = $html->getElementsByTagName('table');
        /** @var DOMElement $table */
        foreach ($tables as $table) {
            $table->setAttribute('class', StyleProvider::get('table'));
        }

        $trs = $html->getElementsByTagName('tr');
        /** @var DOMElement $tr */
        foreach ($trs as $tr) {
            $tr->setAttribute('class', StyleProvider::get('tr'));
        }
        $theads = $html->getElementsByTagName('thead');
        /** @var DOMElement $thead */
        foreach ($theads as $thead) {
            $thead->setAttribute('class', StyleProvider::get('thead'));
        }

        $tbodys = $html->getElementsByTagName('tbody');
        /** @var DOMElement $tbody */
        foreach ($tbodys as $tbody) {
            $tbody->setAttribute('class', StyleProvider::get('tbody'));
        }
        $ths = $html->getElementsByTagName('th');
        /** @var DOMElement $th */
        foreach ($ths as $th) {
            $th->setAttribute('class', StyleProvider::get('th'));
        }

        $tds = $html->getElementsByTagName('td');
        /** @var DOMElement $td */
        foreach ($tds as $td) {
            $td->setAttribute('class', StyleProvider::get('td'));
        }

        return $html;

    }

    private function makeLinks(DOMDocument $html): DOMDocument
    {
        $links = $html->getElementsByTagName('a');
        /** @var DOMElement $link */
        foreach ($links as $link) {
            $attributes = 'noreferer noopener';
            if (array_key_exists('host', parse_url($link->getAttribute('href'))) && $_SERVER['SERVER_NAME'] !== parse_url($link->getAttribute('href'))['host']) {
                $attributes .= ' nofollow';
            }
            $link->setAttribute('rel', $attributes);
            $link->setAttribute('class', StyleProvider::get('link'));
        }

        return $html;
    }
}